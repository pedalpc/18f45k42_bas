PROJECT ?= program
# Orange Pi
#SERIAL_PORT=/dev/ttyS3
#GPIO_PIN=68
# RPi3
# primary UART is on GPIO
# secondary UART is connected to Bluetooth
# full UART is PL011 and is at /dev/ttyAMA0
# mini-UART has reduced feature set and is at /dev/ttyS0
# default settings:
#  PL011 connected to secondary UART (Bluetooth)
#  mini-UART disconnected
#  Linux consle is connected to primary UART (= on GPIO pins)
# To get serial working:
# 1. Turn off Linux console:
#  remove `console=/dev/ttyS0,115200` or `console=/dev/serial0,115200`
#   from `/boot/cmdline.txt`.  Resulting file should look something like:
#   `console=tty1 root=PARTUUID=339a64be-02 rootfstype=ext4 elevator=deadline ...`
# 2. Add 'enable_uart=1' to enable mini-UART & assign to GPIO (i.e., make it the primary UART).
#   SERIAL_PORT will be /dev/ttyS0 in this case.  Bluetooth will remain on the full UART.
# 3. If this doesn't work, add `dtoverlay=bt-disable` to `/boot/config.txt` to make PL011 the
#   primary UART and disable the mini-UART (& Bluetooth).  SERIAL_PORT will be /dev/ttyAMA0.
#   (This is the only way I could get the serial port to reliably function once.)
#SERIAL_PORT=/dev/ttyAMA0
SERIAL_PORT=/dev/ttyS0
GPIO_PIN=23
#BAUD_RATE=115200
# lower baud rate seems to cause fewer web server Tx errors
BAUD_RATE=9600

COMPILER=/opt/GCBASIC/gcbasic
#       Compiler Options
#  -O:output.asm   Set an output filename other than the default
#  -A:assembler    Batch file used to call assembler. If /A:GCASM is
#                         given, GCBASIC will use its internal assembler.
#  -P:programmer   Batch file used to call programmer, or programmer
#	                  name if defined in settings file.
#  -K:{C|A}        Keep original code in assembly output. /K:C will
#                         save comments, /K:A will preserve all input code.
#  -R:format       Write a compilation report. Format can be html, text
#                         or none.
#  -S:settings.ini Specify name of settings file to load
#  -V              Verbose mode
#  -L              Show license
#  -NP             Do not pause on errors. Use with IDEs.
#  -WX             Treat warnings as errors
#  -F             	Skip compilation if up to date hex file found, call
#                         programmer immediately and then exit.
#COMPILER_OPT=-A:GCASM -R:text -K:A -WX
COMPILER_OPT=-A:GCASM -R:none -K:A -WX -V

.PHONY:	flash
$(PROJECT).hex: $(PROJECT).bas
	$(COMPILER) $(COMPILER_OPT) $(PROJECT).bas
	#cat program.report.txt | sed '/[A-Za-z]/p;d'

asm:
	$(COMPILER) $(COMPILER_OPT) $(PROJECT).bas

flash: $(PROJECT).hex
	@# === MUST BE RUN AS SUDO TO WORK ===
	@# commands to turn on GPIO pin to keep power to board and
	@# allow _RESET pin to work.
	@# If you get "tee: /sys/class/gpio/export: Device or resource busy",
	@# run this command first:
	@# 	echo $(GPIO_PIN) | sudo tee /sys/class/gpio/unexport
	echo $(GPIO_PIN) | sudo tee /sys/class/gpio/export
	echo out | sudo tee /sys/class/gpio/gpio$(GPIO_PIN)/direction
	echo 1 | sudo tee /sys/class/gpio/gpio$(GPIO_PIN)/value
	sleep 2
	n16 lvp program $(PROJECT).hex
	# allow time to reset
	@echo "------------------------------------------------------------------------"
	@echo ""
	@echo "			Programming done. Stop pedalling now."
	@echo ""
	@echo "	 The LED will be RED for 10 seconds, followed by 3 GREEN blinks,"
	@echo "         then a 3 second pause."
	@echo ""
	@echo "	 It will then begin briefly blinking red or green once ever 10 seconds."
	@echo ""
	@echo "	 You should resume pedalling when the brief blinks begin."
	@echo ""
	@echo "	 This program will pause for 30 seconds to allow all this to occur."
	@echo ""
	@echo "------------------------------------------------------------------------"
	sleep 30
	echo 0 | sudo tee /sys/class/gpio/gpio$(GPIO_PIN)/value
	echo $(GPIO_PIN) | sudo tee /sys/class/gpio/unexport

test: 	
	@# This tests if the connections to the board and the chip are OK.
	@# === MUST BE RUN AS SUDO TO WORK ===
	@# It sends the commands to turn on GPIO pin to keep power to board and
	@# allow _RESET pin to work (which is necessary for all p14/n16 commands)
	@# If you get "tee: /sys/class/gpio/export: Device or resource busy",
	@# run this command first:
	@# 	echo $(GPIO_PIN) | sudo tee /sys/class/gpio/unexport
	echo $(GPIO_PIN) | sudo tee /sys/class/gpio/export
	echo out | sudo tee /sys/class/gpio/gpio$(GPIO_PIN)/direction
	echo 1 | sudo tee /sys/class/gpio/gpio$(GPIO_PIN)/value
	sleep 2
	n16 lvp id
	@# allow time to reset
	sleep 2
	echo 0 | sudo tee /sys/class/gpio/gpio$(GPIO_PIN)/value
	echo $(GPIO_PIN) | sudo tee /sys/class/gpio/unexport
	@echo "testing done"

dump: 	
	@# Saves firmware on the microcontroller to file
	@# === MUST BE RUN AS SUDO TO WORK ===
	@# It sends the commands to turn on GPIO pin to keep power to board and 
	@# allow _RESET pin to work (which is necessary for all p14/n16 commands)
	@# If you get "tee: /sys/class/gpio/export: Device or resource busy",
	@# run this command first:
	@# 	echo $(GPIO_PIN) | sudo tee /sys/class/gpio/unexport
	echo $(GPIO_PIN) | sudo tee /sys/class/gpio/export
	echo out | sudo tee /sys/class/gpio/gpio$(GPIO_PIN)/direction
	echo 1 | sudo tee /sys/class/gpio/gpio$(GPIO_PIN)/value
	sleep 2
	n16 lvp dump > /tmp/temp.hex
	@# allow time to reset
	sleep 2
	echo 0 | sudo tee /sys/class/gpio/gpio$(GPIO_PIN)/value
	echo $(GPIO_PIN) | sudo tee /sys/class/gpio/unexport
	@echo "testing done"


verify: $(PROJECT).hex	
	# This tests if the chip matches the hex file
	# === MUST BE RUN AS SUDO TO WORK ===
	# It sends the commands to turn on GPIO pin to keep power to board and
	# allow _RESET pin to work (which is necessary for all p14/n16 commands)
	# If you get "tee: /sys/class/gpio/export: Device or resource busy",
	# run this command first:
	# 	echo $(GPIO_PIN) | sudo tee /sys/class/gpio/unexport
	echo $(GPIO_PIN) | sudo tee /sys/class/gpio/export
	echo out | sudo tee /sys/class/gpio/gpio$(GPIO_PIN)/direction
	echo 1 | sudo tee /sys/class/gpio/gpio$(GPIO_PIN)/value
	sleep 2
	n16 lvp verify $(PROJECT).hex
	@# allow time to reset
	sleep 2
	echo 0 | sudo tee /sys/class/gpio/gpio$(GPIO_PIN)/value
	echo $(GPIO_PIN) | sudo tee /sys/class/gpio/unexport
	@echo "testing done"

edit:
	vim $(PROJECT).bas

# don't flash before using usb serial due to the time it takes to enumerate
term:	
	picocom -b $(BAUD_RATE) $(SERIAL_PORT) --logfile /tmp/serial_out.txt

serial: flash
	picocom -b $(BAUD_RATE) $(SERIAL_PORT)
