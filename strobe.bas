' Chip layout for board V4.5
'
'                                 +-------+
'                   Vpp/_MCLR/RE3 |       | RB7/ANB7/PGD/RX*  PGD/RX
'   SOCKET6_A1           RA0/ANA0 |       | RB6/ANB6/PGC/TX*  PGC/TX
'   SOCKET6_ON           RA1/ANA1 |       | RB5/ANB5          SOCKET2_A2
'   SOCKET6_A2           RA2/ANA2 |       | RB4/ANB4          SOCKET2_ON
'                        RA3/ANA3 |       | RB3/ANB3          SOCKET2_A1
'                        RA4/ANA4 |       | RB2/ANB2          SOCKET4_A2
'   GENERATOR_OFF        RA5/ANA5 |       | RB1/ANB1          SOCKET4_ON
'                        RE0/ANE0 |       | RB0/ANB0          SOCKET4_A1
'                        RE1/ANE1 |       | Vdd
'                        RE2/ANE2 |       | Vss
'                             Vdd |       | RD7/AND7          GREEN_LED
'                             Vss |       | RD6/AND6          RED_LED
'   BATTERY_A1/VOLTS     RA7/ANA7 |       | RD5/AND5          SOCKET3_A2
'   BATTERY_A2           RA6/ANA6 |       | RD4/AND4          SOCKET3_ON
'   FAN_A1               RC0/ANC0 |       | RC7/ANC7          SOCKET3_A1
'   FAN_ON               RC1/ANC1 |       | RC6/ANC6          
'   FAN_A2               RC2/ANC2 |       | RC5/ANC5
'   SOCKET5_A1           RC3/ANC3 |       | RC4/ANC4          SOCKET1_A2
'   SOCKET5_ON           RD0/AND0 |       | RD3/AND3          SOCKET1_ON
'   SOCKET5_A2           RD1/AND1 |       | RD2/AND2          SOCKET1_A1
'                                 +-------+
'''************************************************************************
'''@author  Jim Gregory
'''@licence GPL
'''@version 1.01
'''@date    2018-10-07

' TODO:
' shutdown if battery amps are too high either charging or discharging
' enable shutdown from computer
' ----- Configuration

'Chip Settings.
' power consumption vs internal oscillator freq
'4 Mhz  -> 3.45 mA no LED, ~12 mA w/ LED, 9600 B UART
'16 Mhz -> 5.9 mA no LED, ~14 mA w/ LED, 9600 B UART
'64 Mhz -> 15.8 mA no LED, ~23 mA w/ LED, 9600 B UART
 #chip 18f45k42, 16 
#option explicit
#config CLKOUTEN=off                'no clock output
#config WDTE=on                     'enable watchdog timer
#config WDTCPS=WDTCPS_13            '8 sec watchdog timer
#config XINST=off                   'no extended instruction set
#config DEBUG=off                   'no debug
#config BOREN=off                   'no brownout reset
#config FCMEN=off                   'no clock monitoring
#config LVP=on                      'enable low-voltage programming
#config MVECEN=off                  'no interrupt vector table (use legacy)
#config MCLRE=EXTMCLR               'external reset

#startup UARTPPS, 85                'set InitPPS at medium priority
Sub UARTPPS
    'input peripherals map to pin registers,
    'output pins map to peripheral registers
    U1RXPPS = 0x000F                  'RB7 > Rx 
    RB6PPS = 0x0013                   'RB6 > Tx
    RD6PPS = 0x000B                  'RD6 > CCP3
    RD7PPS = 0x000C                  'RD7 > CCP4
End Sub

Sub PWMPPS
    ' 8-bit CCP for PWM
    RD6PPS = 0x000B                  'RD6 > CCP3
    RD7PPS = 0x000C                  'RD7 > CCP4
End Sub

Sub GPIOPPS
    RD6PPS = 0x0000                  'RD6 > GPIO
    RD7PPS = 0x0000                  'RD7 > GPIO
End Sub

#define GREEN_LED       portD.7
#define RED_LED         portD.6


' ----- Constants
#define SAMPLE_SIZE 32      '# of sampling periods per second
#define RSHIFT 5            'amount to right-shift to calculate averages 
#define DEVICE_COUNT 8      '# of powered devices plus battery
#define AUTO 2              '3 possible values for power_setting 
#define ON 1
#define OFF 0 
#define MIN_DRAIN_TIME 30       'min allowable drain time, in seconds
#define LED_START_STROBE 30     'start strobing LED after battery drains this long
#define LED_DRAIN_PERIOD 10     'secs until LED starts to blink
#define AUTO_OFF_PERIOD 15      'secs until auto devices shut off
#define RESTART_WAIT_PERIOD 5   'secs to wait until system restarts
#define MAX_OVERVOLT_PERIOD 3   'max secs until overvolt relay trips
#define BLINK_RATE 4            '# of LED blinks per second
#define ADC_VREF 2.048
#define ADC_RESOLUTION 4096
#define R1 1000             'R1 voltage divider resistor (in Ohms)
#define R2 33000            'R2 voltage divider resistor (in Ohms)
#define R_SENSE 0.04        'current sense resistor (in Ohms)
#define AWAKE_AMPS 1        'min battery current reqd to awake from sleep
' When charging, battery voltage is ~.3 V lower than system voltage
' Clean Republic battery has max recommended charge voltage = 14 V
#define VOLTS_WARNING 14.2      'voltage level to trigger a warning
' Clean Republic battery shutoffs at 15 V max
' Set generator to disconnect at 14.5 V to prevent inverter shutdown 
' (which supposedly is 15-16V): 
#define MAX_VOLTS 14.5          
' When discharging, battery voltage is ~.3 V greater than system voltage
' Clean Republic battery shuts off at 9 V min (tech support 
'    recommends > 10 V)
' Set shutdown to occur at 10 V
#define MIN_VOLTS 10
#script 
    ADC_MULTIPLIER = R1/(R1+R2) / ADC_VREF * ADC_RESOLUTION
    VOLTS_WARNING_ADC = Int(VOLTS_WARNING * ADC_MULTIPLIER)
    MAX_VOLTS_ADC = Int(MAX_VOLTS * ADC_MULTIPLIER)
    MIN_VOLTS_ADC = Int(MIN_VOLTS * ADC_MULTIPLIER)
    AWAKE_ADC = INT(AWAKE_AMPS * R_SENSE / ADC_VREF * ADC_RESOLUTION)
    TIMER1_PERIOD = INT(31000/SAMPLE_SIZE)
    TIMER1_COUNTER_START = INT(65536 - TIMER1_PERIOD)
    'TIMER1_COUNTER_START = 65536 - INT(31000/SAMPLE_SIZE/64)
#endscript

'Setup Serial port
#define USART_BAUD_RATE 9600
'#define USART_TX_BLOCKING
#define USART_BLOCKING
#define SerInPort portb.7       'USART Rx pin
#define SerOutPort portb.6      'USART Tx pin
#define GREEN 1
#define RED 2
#define YELLOW 3

' PWM channels controllng LED color
#define GREEN_PWM 4
#define RED_PWM 3

Dir GREEN_LED out
Dir RED_LED out
Dir SerInPort in        
Dir SerOutPort out     
    
Dim i as integer
Dim drain_timer as Word
Dim sample_counter as Byte
Dim adc_volts as Word
Dim brightness as Byte

Sub PulseLED(in color As Integer)
    #define MAX_LED_BRIGHTNESS 128
    Dim brightness As Integer
    Dim is_increasing As byte
    brightness = 1
    is_increasing = 1
    Do While brightness > 0
        If color = RED Then
            HPWMOff(GREEN_PWM)
            HPWM RED_PWM, 10, brightness
            If brightness >= MAX_LED_BRIGHTNESS Then
                HSerPrintStringCRLF "RED"
            End If
        Else If color = GREEN Then
            HPWMOff(RED_PWM)
            HPWM GREEN_PWM, 10, brightness
            If brightness >= MAX_LED_BRIGHTNESS Then
                HSerPrintStringCRLF "GREEN"
            End If
        Else If color = YELLOW Then
            HPWM RED_PWM, 10, brightness/4 'reduce red to make LED more yellow
            HPWM GREEN_PWM, 10, brightness
            If brightness >= MAX_LED_BRIGHTNESS Then
                HSerPrintStringCRLF "YELLOW"
            End If
        Else
            ' LED off
            HPWM RED_PWM, 10, 0
            HPWM GREEN_PWM, 10, 0
            If brightness >= MAX_LED_BRIGHTNESS Then
                HSerPrintStringCRLF "OFF"
            End If
        End If
        If is_increasing = 1 Then
            brightness = brightness + 4 
        Else
            brightness = brightness - 4
        End If
        If brightness >= MAX_LED_BRIGHTNESS Then
            is_increasing = 0   'ramp down brighness
        End if
        clrwdt          'assembly code must be lower case!
        Wait 33 ms
    Loop
End Sub

Sub StrobeLED(in sample_counter as Byte, in drain_timer as Word, in adc_volts as Integer)
    ' strobe LED based on volts_adc
    ' full red @ MIN_VOLTS_ADC 
    ' full green @ ~VOLTS_WARNING
    ' LED increases in brightness for 1 sec, decreases in brightness for 1 sec, 
    '   then pauses for remainder of STROBE_PERIOD
    #define STROBE_PERIOD 4     'seconds to fully strobe LED, including pause
    Dim full_red_brightness, full_green_brightness As Word
    Dim red_brightness, green_brightness, counter As Word
    ' calculate color at t=1sec based on the current voltage reading
    full_green_brightness = adc_volts - MIN_VOLTS_ADC
    If full_green_brightness > 255 Then
        full_green_brightness = 255
    End If
    full_red_brightness = 255 - full_green_brightness 
    ' adjust the brightness based on the current sampling time
    Select Case drain_timer % STROBE_PERIOD         
        Case 1: counter = sample_counter                'increasing brightness 
        Case 2: counter = SAMPLE_SIZE-sample_counter    'decreasing brightness 
        Case Else: counter = 0                          'LED off
    End Select
    green_brightness = FnLSR(full_green_brightness, 5) * counter
    red_brightness = FnLSR(full_red_brightness, 6) * counter
'    If adc_volts > 0 Then
'        HSerPrint "V = "
'        HserPrint adc_volts
'        HSerPrint ",c = "
'        HserPrint counter
'        HSerPrint ",FR = "
'        HSerPrint full_red_brightness
'        HSerPrint ",FG = "
'        HSerPrint full_green_brightness
'        HSerPrint ",R = "
'        HSerPrint red_brightness
'        HSerPrint ",G = "
'        HSerPrint green_brightness
'        HSerPrintCRLF
'    End If

    HPWM RED_PWM, 10, red_brightness
    HPWM GREEN_PWM, 10, green_brightness
End Sub

Sub BlinkLED(in volts as Integer, in battery_amps as Integer, in drain_timer as Word) 
    blink_counter++
    If blink_counter = 4 Then
        blink_counter = 0
    End If
    If volts > VOLTS_WARNING_ADC Then
        ' pedalling too fast -- blink fast green/red
        If blink_counter % 2 = 0 Then
            TurnLED(RED)
        Else
            TurnLED(GREEN)
        End If
    Else If
        Else If battery_amps < 0 Then
            Else If drain_timer >= max_drain_time Then
                ' system is asleep - turn LED off
                TurnLED(OFF)
            Else If drain_timer > LED_START_STROBE Then
                ' no one apparently pedalling - strobe slowly 
                ' with color based on battery voltage

            Else If drain_timer > LED_DRAIN_PERIOD Then
                ' battery draining for awhile - blink red
                If blink_counter < 2 Then
                    TurnLED(RED)
                Else
                    TurnLED(OFF)
            Else
                ' battery draining - turn solid yellow
                TurnLED(YELLOW)
            End If
        Else
            ' battery charging - turn solid green
            TurnLED(GREEN)
        End If
    End If
End Sub

Sub TurnLED(color as Byte, Optional brightness As Byte = 255)
    Select Case color
    Case RED:
        HPWMOff(GREEN_PWM)
        HPWM RED_PWM, 10, brightness
    Case GREEN:
        HPWMOff(RED_PWM)
        HPWM GREEN_PWM, 10, brightness
    Case YELLOW:
        HPWM RED_PWM, 10, brightness/4 'reduce red to make LED more yellow
        HPWM GREEN_PWM, 10, brightness
    Case Else:
        ' LED off
        HPWM RED_PWM, 10, 0
        HPWM GREEN_PWM, 10, 0
    End Select
End Sub

Do
'    GPIOPPS()
'    For i = 8 to 9 
'        HSerPrint i
'        HSerPrint ": blinking" 
'        HserPrintCRLF
'        GREEN_LED = ON
'        wait 1 sec
'        GREEN_LED = OFF
'        wait 1 sec
'    Next
'    PWMPPS()
'    For i = 10 to 13 
'        HSerPrint i
'        HSerPrint ": " 
'        If i < 11 Then
'            PulseLED(OFF)
'        Else If i < 12 Then
'            PulseLED(RED)
'        Else If i < 13 Then
'            PulseLED(YELLOW)
'        Else
'            PulseLED(GREEN)
'        End If
'    Next
'    HPWMOff(RED_PWM)
'    HPWMOff(GREEN_PWM)
    For i = 0 to 8
        adc_volts = 600 + (i * 32)
'        HSerPrint adc_volts
'        HSerPrintCRLF
        For drain_timer = 1 to 4
            clrwdt
'            HSerPrint adc_volts
'            HserPrint ": "
'            HserPrint drain_timer
'            HSerPrintCRLF 
            For sample_counter = 0 to 31
                StrobeLED(sample_counter, drain_timer, adc_volts)
                Wait 33 ms
            Next 
        Next
    Next
Loop
