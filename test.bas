' Chip layout for board V4.6
'
'                                 +-------+
'                   Vpp/_MCLR/RE3 |       | RB7/ANB7/PGD/RX*  PGD/RX
'   SOCKET7_A1           RA0/ANA0 |       | RB6/ANB6/PGC/TX*  PGC/TX
'   SOCKET7_A2           RA1/ANA1 |       | RB5/ANB5          SOCKET1_A1
'   Vref-                RA2/ANA2 |       | RB4/ANB4          SOCKET1_A2
'   SOCKET1_ON           RA3/ANA3 |       | RB3/ANB3          SOCKET3_A1
'   SOCKET3_ON      RA4/ANA4/PWM8 |       | RB2/ANB2          SOCKET3_A2
'   SOCKET5_ON      RA5/ANA5/PWM7 |       | RB1/ANB1          SOCKET5_A1
'   SOCKET7_ON           RE0/ANE0 |       | RB0/ANB0          SOCKET5_A2
'   BATTERY_A2           RE1/ANE1 |       | Vdd
'   BATTERY_A1/VOLTS     RE2/ANE2 |       | Vss
'                             Vdd |       | RD7/AND7/PWM6     GREEN_LED
'                             Vss |       | RD6/AND6          SOCKET6_A2
'   RED_LED         RA7/ANA7/PWM5 |       | RD5/AND5          SOCKET6_A1
'   GENERATOR_OFF        RA6/ANA6 |       | RD4/AND4/CCPPWM3  SOCKET6_ON
'   FAN_ON       RC0/ANC0/CCPPWM1 |       | RC7/ANC7          SOCKET4_A2
'   FAN_A2               RC1/ANC1 |       | RC6/ANC6          SOCKET4_A1
'   FAN_A1               RC2/ANC2 |       | RC5/ANC5/CCPPWM2  SOCKET4_ON 
'   SOCKET8_ON           RC3/ANC3 |       | RC4/ANC4          SOCKET2_A2
'   SOCKET8_A2           RD0/AND0 |       | RD3/AND3          SOCKET2_A1
'   SOCKET8_A1           RD1/AND1 |       | RD2/AND2          SOCKET2_ON
'                                 +-------+
'''************************************************************************

' ----- Configuration
'Chip Settings.
' power consumption vs internal oscillator freq
'4 Mhz  -> 3.45 mA no LED, ~12 mA w/ LED, 9600 B UART, can do 328 samples/sec
'8 Mhz  -> 630 samples/sec     
'16 Mhz -> 5.9 mA no LED, ~14 mA w/ LED, 9600 B UART, can do 1130 samples/sec
'64 Mhz -> 15.8 mA no LED, ~23 mA w/ LED, 9600 B UART
#chip 18f45k42, 16 
#option explicit
#config CLKOUTEN=off                'no clock output
#config WDTE=on                     'enable watchdog timer
#config WDTCPS=WDTCPS_13            '8 sec watchdog timer
#config XINST=off                   'no extended instruction set
#config DEBUG=off                   'no debug
#config BOREN=off                   'no brownout reset
#config FCMEN=off                   'no clock monitoring
#config LVP=on                      'enable low-voltage programming
#config MVECEN=off                  'no interrupt vector table (use legacy)
#config MCLRE=EXTMCLR               'external reset
#config FEXTOSC=off                 'extosc *MUST* be off for ANA7 to function!

#startup InitPPS, 85                'set InitPPS at medium priority

Sub InitPPS
    'sets pins to PWM (see table 17-2, p 283 in datasheet)
    U1RXPPS = 0x000F               'RB7 > Rx
    RB6PPS = 0x0013                'RB6 > Tx

    RD7PPS = 0x0E                  'GREEN_LED: RD7 > PWM6
    RA7PPS = 0x0D                  'RED_LED: RA7 > PWM5
    RA4PPS = 0x10                  'SOCKET3_ON: RA4 > PWM8
    RC5PPS = 0x0A                  'SOCKET4_ON: RC5 > CCP2
    RA5PPS = 0x0F                  'SOCKET5_ON: RA5 > PWM7
    RD4PPS = 0x0B                  'SOCKET6_ON: RD4 > CCP3
    RC0PPS = 0x09                  'FAN_ON: RC0 > CCP1
End Sub

Sub PWMPPS
    'sets LED pins to PWM (for strobing)
    ' 8-bit CCP for PWM
    RD7PPS = 0x0E                  'GREEN_LED: RD7 > PWM6
    RA7PPS = 0x0D                  'RED_LED: RA7 > PWM5
End Sub

Sub GPIOPPS
    'sets LED pins to digital I/O (for blinking)
    RD7PPS = 0x00                  'GREEN_LED: RD7 > GPIO
    RA7PPS = 0x00                  'RED_LED: RA7 > GPIO
End Sub

#define DEVICE_COUNT 7
'Setup Serial port
#define USART_BAUD_RATE 9600
' Note: USART_TX_BLOCKING will not read input properly!
#define USART_BLOCKING
#define SerInPort portb.7       'USART Rx pin
#define SerOutPort portb.6      'USART Tx pin
' PWM channels controllng LED color
#define GREEN_PWM 6
#define RED_PWM 5
'device ON/OFF ports
#define GREEN_LED       portD.7
#define RED_LED         portA.7
#define FAN_ON          portC.0
#define SOCKET3_ON      portA.4
#define SOCKET5_ON      portA.5
#define SOCKET4_ON      portC.5
#define SOCKET6_ON      portD.4

Dim i,j,counter,temp_byte As Byte
Dim input_string As String
Dim sample_counter As Byte
Dim drain_timer As Word

' set I/O pin directions
Dir SerInPort in        
Dir SerOutPort out     
Dir GREEN_LED out
Dir RED_LED out
Dir FAN_ON  out
Dir SOCKET3_ON out
Dir SOCKET5_ON out
Dir SOCKET4_ON out
Dir SOCKET6_ON out

Sub set_device_duty_cycle(In socket As Byte, In duty_cycle As Word)
    Dim temp_duty_cycle As Word
    ' duty_cycle ranges from 0-256, so scale by (T2PR + 1) * 4/256 = 2, then left-shift by 6
    ' (2 ^ 6 = 64) to left-align format
    ' NOTE: additional adjustment is required if scale factor = 16, since the 10-bit
    '   duty cycle will overflow when duty_cycle = 64.
    temp_duty_cycle = duty_cycle * 2 * 64

    Select Case socket
        Case 1
            'PWM 5
            PWM5DCH = temp_duty_cycle_H
            PWM5DCL = temp_duty_cycle
        Case 2
            'PWM 6
            PWM6DCH = temp_duty_cycle_H
            PWM6DCL = temp_duty_cycle
        Case 3
            'PWM 8
            PWM8DCH = temp_duty_cycle_H
            PWM8DCL = temp_duty_cycle
        Case 4
            'CCPPWM 2
            CCPR2H = temp_duty_cycle_H
            CCPR2L = temp_duty_cycle
        Case 5
            'PWM 7
            PWM7DCH = temp_duty_cycle_H
            PWM7DCL = temp_duty_cycle
        Case 6
            'CCPPWM 3
            CCPR3H = temp_duty_cycle_H
            CCPR3L = temp_duty_cycle
        Case DEVICE_COUNT
            'CCPPWM 1
            CCPR1H = temp_duty_cycle_H
            CCPR1L = temp_duty_cycle
    End Select
'    HserPrint socket
'    HserPrint "-"
'    HserPrint temp_duty_cycle
'    HserPrint "("
'    HserPrint duty_cycle
'    HserPrint ")"
'    HserPrint ":"
'    HSerPrint temp_duty_cycle_H
'    HserPrint ", "
'    HSerPrint [byte]temp_duty_cycle
'    HserPrint "; "
'    HserPrintCRLF

End Sub
    
'Frequencies below 500 Hz with maximum duty cycle
'values evenly divisible by 64:
'                        TMR2
'  PWM freq      T2PR  prescale
'---------------------------------------------------------------------
'    122 Hz       255       128
'    130 Hz       239       128
'    140 Hz       223       128
'    150 Hz       207       128
'    163 Hz       191       128
'    178 Hz       175       128
'    195 Hz       159       128
'    217 Hz       143       128
'    244 Hz       127       128
'    260 Hz       239        64
'    279 Hz       111       128
'    300 Hz       207        64
'    326 Hz        95       128
'    355 Hz       175        64
'    391 Hz        79       128
'    434 Hz       143        64
'    488 Hz        63       128

' Use Timer 2 on PWM 8 & 7 (sockets 3 & 5), Timer 4 on PWM 6 & 5 (green and red LEDs)
CCPTMRS1 = 0b01011010

' Set CCPxPWM on, use left-aligned format, and select PWM mode
CCP1CON = 0b10011100
CCP2CON = 0b10011100
CCP3CON = 0b10011100

' Load T2PR & T4PR with PWM period value
T2PR = 127 
T4PR = 127

' preload duty_cycle_H and duty_cycle_L registers 
For i = 1 to DEVICE_COUNT
    set_device_duty_cycle(i, 0)
Next

' Clear Timer 2 & Timer 4 interrupt flag bit
PIR4.TMR2IF = 0
PIR7.TMR4IF = 0

' Set Timer 2 and Timer 4 clock source to FOSC/4
T2CLKCON = 1 
T4CLKCON = 1 

' enable Timer 2, set pre-scaler to 1:128, post-scaler to 1:1 to get 244 Hz
T2CON = 0b11110000
' enable Timer 4, set pre-scaler to 1:8, post-scaler to 1:1 to get 2083 kHz
'T4CON = 0b10110000
T4CON = 0b11110000

' enable PWMs 5-8 (CCP PWMs were enabled above in CCPxCON registers)
PWM5CON.7 = ON
PWM6CON.7 = ON
PWM7CON.7 = ON
PWM8CON.7 = ON

#define SAMPLE_SIZE 32 
#define MAX_DRAIN_TIME 300
#define ADC_VREF 2.048          'ADC voltage reference
#define ADC_RESOLUTION 4096     'ADC resolution (4096 = 12 bit)
#define R1 1000             'R1 voltage divider resistor (in Ohms)
#define R2 33000            'R2 voltage divider resistor (in Ohms)
#script
    ADC_MULTIPLIER = R1/(R1+R2) / ADC_VREF * ADC_RESOLUTION
    'should be 1/32 sec for 1:1 prescaler
    TIMER1_PERIOD = INT(31000/SAMPLE_SIZE)
    TIMER1_COUNTER_START = INT(65536 - TIMER1_PERIOD)
#endscript

InitializeInterruptLoopTimer()
sample_counter = 0
drain_timer = 0

Sub InitializeInterruptLoopTimer()
    On Interrupt Timer1Overflow Call BlinkLED
    InitTimer1(LFINTOSC, PS1_1)
    SetTimer(1, TIMER1_COUNTER_START)
    StartTimer(1)
End Sub

Sub BlinkLED()
    'colors:
    ' red: G:R 0:1
    ' orange: G:R 2:1
    ' yellow: G:R 8:1
    ' green: G:R 1:0
    clrwdt          'assembly code must be lower case!
    TMR1IF = 0
    SetTimer(1,TIMER1_COUNTER_START)
    Select Case drain_timer / 8
        Case 0
            StrobeLED(sample_counter, drain_timer, 770)
        Case 1
            StrobeLED(sample_counter, drain_timer, 760)
        Case 2
            StrobeLED(sample_counter, drain_timer, 730)
        Case 3
            StrobeLED(sample_counter, drain_timer, 670)
        Case 4
            GPIOPPS()
            GREEN_LED = ON
            RED_LED = OFF
        Case 5
            GPIOPPS()
            GREEN_LED = OFF
            RED_LED = ON
        Case Else
            PWMPPS()
            drain_timer = 0
    End Select
    sample_counter += 1
    If sample_counter = 32 Then
        drain_timer += 1
        sample_counter = 0
    End If
'    j = j + 4
'    If j = 64 Then
'        j = 0
'        i = i + 4
'        If i = 64 Then
'            i = 0
'        End If
'    End If
End Sub

Sub StrobeLED(In sample_counter As Byte, In drain_timer As Word, In adc_volts As Word)
    ' strobe LED based on voltage
    ' LED increases in brightness for 1 sec, decreases in brightness for 1 sec, 
    '   then pauses for remainder of STROBE_PERIOD
    ' color is determined by volts ADC reading:
    ' full red @ MIN_VOLTS_ADC 
    ' full green @ ~VOLTS_WARNING
    #define STROBE_PERIOD 8     'seconds to fully strobe LED, including pause
    Dim red_multiplier, green_multiplier as Word
    Dim red_brightness, green_brightness, strobe_counter, pwm_multiplier as Word
    If drain_timer = max_drain_time Then
        'turn strobe off after all has shutdown
        set_device_duty_cycle(1, 0) 'red
        set_device_duty_cycle(2, 0) 'green
        Return
    End If
    ' calculate color at t=1sec based on the current voltage reading
    #script
        'ADC_MULTIPLIER = ~58.82
        GREEN_VOLTS = INT(13.0 * ADC_MULTIPLIER)    '13.0 -> 765
        YELLOW_VOLTS = INT(12.5 * ADC_MULTIPLIER)   '12.5 -> 735
        ORANGE_VOLTS = INT(11.7 * ADC_MULTIPLIER)   '11.7 -> 688
    #endscript
    If adc_volts > GREEN_VOLTS Then
        green_multiplier = 8
        red_multiplier = 0
    Else If adc_volts > YELLOW_VOLTS Then
        green_multiplier = 8
        red_multiplier = 1
    Else If adc_volts > ORANGE_VOLTS Then
        green_multiplier = 2
        red_multiplier = 1
    Else    
        ' full red
        green_multiplier = 0
        red_multiplier = 2
    End If
    ' adjust the brightness based on the current sampling time
    strobe_counter = drain_timer % STROBE_PERIOD
    Select Case strobe_counter
        Case 1: pwm_multiplier = sample_counter                'increasing brightness 
        Case 2: pwm_multiplier = SAMPLE_SIZE-sample_counter    'decreasing brightness 
        Case Else: pwm_multiplier = 0                          'LED off
    End Select
    ' set color based on voltage AND time
    ' assumes 0-256 duty cycle
    green_brightness = green_multiplier * pwm_multiplier
    red_brightness = red_multiplier * pwm_multiplier
    set_device_duty_cycle(1, red_brightness) 'red
    set_device_duty_cycle(2, green_brightness) 'green
End Sub


Do
    HSerGetString input_string 
    ' the command is the first character of the input string
    ' response_string is appended to the next outgoing data stream
    Select Case input_string(1)
        Case "G"
            temp_byte = Val(Mid(input_string,2))
            If temp_byte > 64 Then
                HSerPrint "Too Large"
                HSerPrintCRLF
            Else
                i = temp_byte
            End if
        Case "R"
            temp_byte = Val(Mid(input_string,2))
            If temp_byte > 64 Then
                HSerPrint "Too Large"
                HSerPrintCRLF
            Else
                j = temp_byte
            End if
        Case Else
            HSerPrint "Invalid - '" 
            HSerPrint CHR(input_string(1)) 
            HSerPrint "'"
            HSerPrintCRLF
    End Select
Loop
