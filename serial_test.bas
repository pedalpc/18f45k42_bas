' ----- Configuration
'Chip Settings.
#chip 18f45k42, 16 
#option explicit
#config CLKOUTEN=off                'no clock output
#config WDTE=on                     'enable watchdog timer
#config WDTCPS=WDTCPS_13            '8 sec watchdog timer
#config XINST=off                   'no extended instruction set
#config DEBUG=off                   'no debug
#config BOREN=off                   'no brownout reset
#config FCMEN=off                   'no clock monitoring
#config LVP=on                      'enable low-voltage programming
#config MVECEN=off                  'no interrupt vector table (use legacy)
#config MCLRE=EXTMCLR               'external reset
#config FEXTOSC=off                 'extosc *MUST* be off for ANA7 to function!

#startup InitPPS, 85                'set InitPPS at medium priority

'Setup Serial port
#define USART_BAUD_RATE 115200
#define USART_BLOCKING
#define SerInPort portb.7       'USART Rx pin
#define SerOutPort portb.6      'USART Tx pin
Dir SerInPort in        
Dir SerOutPort out     

#define GREEN_LED portE.0
#define RED_LED portE.1
Dir GREEN_LED out
Dir RED_LED out

Sub InitPPS
    ' Configure serial port pins
    'input peripherals map to pin registers,
    'output pins map to peripheral registers
    'Module: UART1
    U1RXPPS = 0x000F                  'RB7 > Rx 
    RB6PPS = 0x0013                   'RB6 > Tx
End Sub
    
    
'Main program commences here.. everything before this is board setup

Do
    clrwdt          'assembly code must be lower case!
    Wait 1 sec
    Set GREEN_LED on
    Set RED_LED off
    HserPrintStringCRLF "Hello World! 0123456789"
    Wait 1 sec
    Set GREEN_LED off
    Set RED_LED on
Loop
