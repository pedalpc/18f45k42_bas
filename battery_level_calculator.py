#! /usr/bin/python3
'''
This script calculates the approximate state of charge (SoC) of a LiFePO4 battery for a range of ADC values between two voltage levels.

It assumes the SoC varies linearly between each pair of points

Voltage vs SofC data from graph on p. 11 of:

 https://www.cse.anl.gov/us-china-workshop-2011/pdfs/batteries/LiFePO4%20battery%20performances%20testing%20for%20BMS.pdf
'''
volts = [
    2.750,      # min battery voltage = 11 V
    3.000,
    3.197,
    3.240,
    3.264,
    3.290,
    3.295,
    3.298,
    3.311,
    3.333,
    3.334,
    3.341,
    3.4,
    3.625      # max battery voltage = 14.5

]              

percent = [
    0,
    0,
    10,
    20,
    30,
    40,
    50,
    60,
    70,
    80,
    90,
    98,
    100,
    100,
]        

FULL_BATTERY_CAPACITY = 10      # in Ah
VREF = 2.048                    # ADC voltage at full resolution
ADC_RESOLUTION = 4096           # 12-bit resolution 
R_SHUNT = .04                   # current-measuring shunt resistance
R1 = 1000                       # resistor 1 of system voltage divider
R2 = 33000                      # resistor 2 of system voltage divider

def volts_to_adc(volts):
    # convert a voltage reading to ADC value
    return round(4 * volts * R1/(R1 + R2) / VREF * ADC_RESOLUTION)

def adc_to_volts(adc):
    # convert an ADC value to battery voltage
    return round(adc * (R1 + R2) / R1 * VREF / ADC_RESOLUTION, 2)

def pct_to_soc(percent):
    # convert a cell's percent SOC to a battery's SOC in "ADCbit-secs" 
    # (microcontroller-friendly version of Ahr)
    return int(percent/100 * FULL_BATTERY_CAPACITY * 3600 * R_SHUNT / VREF * ADC_RESOLUTION) 
    #return int(percent/100 * FULL_BATTERY_CAPACITY * 3600 * R_SHUNT / VREF) 

row_count = 0
for i in range(len(volts)-1):
    v1 = volts_to_adc(volts[i])
    v2 = volts_to_adc(volts[i+1])
    soc1 = pct_to_soc(percent[i])
    soc2 = pct_to_soc(percent[i+1])
    for x in range(v1,v2):
        soc = int((x-v1)/(v2-v1)*(soc2 - soc1) + soc1)
        # print the ADC value and corrensponding SOC value (for debugging)
        print("    %d   ' %d %.3f V" % ( soc, x, adc_to_volts(x)))
        # print only the SOC value (for generating lookup table)
        #print(soc)
        row_count += 1
        #if soc == pct_to_soc(100): break

print("' first value (0%%): %d %.3f V %d" % (volts_to_adc(volts[0]), 
        4*volts[0], pct_to_soc(percent[0])))
print("' last value (100%%): %d %.3f V %d" % (x, 4*volts[-1], soc))
print("' total rows: ", row_count)
