' Chip layout for board V4.7.
'
'                                 +-------+
'                   Vpp/_MCLR/RE3 |       | RB7/ANB7/PGD/RX*  PGD/RX
'   SOCKET7_A1           RA0/ANA0 |       | RB6/ANB6/PGC/TX*  PGC/TX
'   SOCKET7_A2           RA1/ANA1 |       | RB5/ANB5          SOCKET1_A1
'   Vref-                RA2/ANA2 |       | RB4/ANB4          SOCKET1_A2
'   SOCKET1_ON           RA3/ANA3 |       | RB3/ANB3          SOCKET3_A1
'   SOCKET3_ON      RA4/ANA4/PWM8 |       | RB2/ANB2          SOCKET3_A2
'   SOCKET5_ON      RA5/ANA5/PWM7 |       | RB1/ANB1          SOCKET5_A1
'   SOCKET7_ON           RE0/ANE0 |       | RB0/ANB0          SOCKET5_A2
'   BATTERY_A2           RE1/ANE1 |       | Vdd
'   BATTERY_A1/VOLTS     RE2/ANE2 |       | Vss
'                             Vdd |       | RD7/AND7/PWM6     GREEN_LED
'                             Vss |       | RD6/AND6          SOCKET6_A2
'   RED_LED         RA7/ANA7/PWM5 |       | RD5/AND5          SOCKET6_A1
'   GENERATOR_OFF        RA6/ANA6 |       | RD4/AND4/CCPPWM3  SOCKET6_ON
'   FAN_ON       RC0/ANC0/CCPPWM1 |       | RC7/ANC7          SOCKET4_A2
'   FAN_A2               RC1/ANC1 |       | RC6/ANC6          SOCKET4_A1
'   FAN_A1               RC2/ANC2 |       | RC5/ANC5/CCPPWM2  SOCKET4_ON 
'   SOCKET8_ON           RC3/ANC3 |       | RC4/ANC4          SOCKET2_A2
'   SOCKET8_A2           RD0/AND0 |       | RD3/AND3          SOCKET2_A1
'   SOCKET8_A1           RD1/AND1 |       | RD2/AND2          SOCKET2_ON
'                                 +-------+
'''************************************************************************
'''@author  Jim Gregory
'''@licence GPL
'''@version 4.7.0
'''@date    2020-05-01

' TODO:
'   adjust duty cycle input values for final 256 values 
' turn on PWM_MODE devices when battery voltage exceeds limits
' adjust pwm_mode_duty_cycle dynamically based on battery voltage (PID control)
' shutdown if battery amps are too high discharging
' enable shutdown from computer

' ----- Configuration
'Chip Settings.
' power consumption vs internal oscillator freq
'4 Mhz  -> 3.45 mA no LED, ~12 mA w/ LED, 9600 B UART, can do 328 samples/sec
'8 Mhz  -> 630 samples/sec     
'16 Mhz -> 5.9 mA no LED, ~14 mA w/ LED, 9600 B UART, can do 1130 samples/sec
'64 Mhz -> 15.8 mA no LED, ~23 mA w/ LED, 9600 B UART
#chip 18f45k42, 16 
#option explicit
#config CLKOUTEN=off                'no clock output
#config WDTE=on                     'enable watchdog timer
#config WDTCPS=WDTCPS_13            '8 sec watchdog timer
#config XINST=off                   'no extended instruction set
#config DEBUG=off                   'no debug
#config BOREN=off                   'no brownout reset
#config FCMEN=off                   'no clock monitoring
#config LVP=on                      'enable low-voltage programming
#config MVECEN=off                  'no interrupt vector table (use legacy)
#config MCLRE=EXTMCLR               'external reset
#config FEXTOSC=off                 'extosc *MUST* be off for ANA7 to function!

#startup InitPPS, 85                'set InitPPS at medium priority

' ----- Constants
#define TEST_MODE 0         'used to test on breadboard without reading ADCs
#define SAMPLE_SIZE 32      '# of sampling periods per second
#define RSHIFT 5            'amount to right-shift to calculate averages 
#define DEVICE_COUNT 9      '# of powered devices (power sockets plus fan)
#define PWM_MODE 3          '4 possible values for power_setting 
#define AUTO_OFF 2              
#define ON 1
#define OFF 0 
#define MIN_DRAIN_TIME 30       'min allowable drain time, in seconds
#define LED_DRAIN_PERIOD 10     'secs until LED starts to blink
#define BLINK_RATE 4            '# of LED blinks per second
#define LED_START_STROBE 20     'secs until LED begins strobing 
#define STROBE_PERIOD 10        'secs for a full up->down->off strobe cycle
#define AUTO_OFF_PERIOD 15      'secs until auto devices shut off
#define RESTART_WAIT_PERIOD 5   'secs to wait until system restarts
#define MAX_OVERVOLT_PERIOD 3   'max secs until overvolt relay trips
#define ADC_VREF 2.048          'ADC voltage reference
#define ADC_RESOLUTION 4096     'ADC resolution (4096 = 12 bit)
#define BATTERY_AH 10       'battery capacity in amp-hours
#define R1 1000             'R1 voltage divider resistor (in Ohms)
#define R2 33000            'R2 voltage divider resistor (in Ohms)
#define R_SENSE 0.04        'current sense resistor (in Ohms)
#define CHARGING_THRESHOLD_AMPS 0.25  'min battery current required to charge
' When charging, battery voltage is ~.3 V lower than system voltage
' Clean Republic (aka Dakota Lithium) battery has max recommended charge 
' voltage = 14 V
#define VOLTS_WARNING_HIGH 14.3      'minimum voltage to trigger LED warning behavior
' battery voltage at ~30% state-of-charge
#define VOLTS_WARNING_LOW 12.5       
' Battery voltage above which devices won't shutoff when battery is discharging.
' This prevents shutoffs when battery is full and charging very little.
' Must be high enough to prevent devices from turning back on after shutdown
#define MIN_DISCHARGE_THRESHOLD_VOLTS 14.1
' Clean Republic battery shutoffs at 15 V max
' Set generator to disconnect at 14.5 V to prevent inverter shutdown 
' (which supposedly is 15-16V): 
#define MAX_VOLTS 14.6  'minimum volts to open relay, disconnecting generator
' When discharging, battery voltage is ~.3 V greater than system voltage
' Clean Republic battery shuts off at 9 V min (tech support 
'    recommends > 10 V)
' Set shutdown to occur at 10.2 V:
#define MIN_VOLTS 10.2
' Maximum amps allowed when charging the battery
' (this can be high if the battery is dead!)
#define MAX_AMPS 5.0    'relay opens when battery amps > MAX_AMPS
' Maximum current allowed into battery when it is full (i.e., maximum current at
' float voltage).  For most LiFePO4 batteries, this is usually considered to be 
' 0.1 C amps, where C = battery capacity in amp-hours. Since the PedalPC uses a 
' 10 AH battery, this is assumed to be 1.0 A.
#define MAX_FLOAT_AMPS = 1.0
' Voltage when battery is considered full.  The voltage must be >= this value
' AND the current must be <= MAX_FLOAT_AMPS for the battery to be fully charged.
' When both conditions are met, the battery is "float" charging.
#define MIN_FLOAT_VOLTS = 14.3
' The battery is considered fully charged when the generator disconnects after it
' has exceeded VOLTS_WARNING_HIGH __AND__ the its charging current has fallen below 
' MAX_FLOAT_AMPS for this many periods
#define SOC_RESET_TIME_PERIOD 30
' PWM constants
#define MAX_DUTY_CYCLE 256 
'#define PWM_FREQ 60 
#script 
    ' generator relay is the the last element of the on/off power_setting array
    GENERATOR_RELAY = DEVICE_COUNT + 1
    ' battery ADC is the last element of the adc array
    BATTERY = DEVICE_COUNT + 1
    ' the fan is the second-to-last element in both arrays
    FAN = DEVICE_COUNT
    ADC_MULTIPLIER = R1/(R1+R2) / ADC_VREF * ADC_RESOLUTION
    VOLTS_WARNING_HIGH_ADC = Int(VOLTS_WARNING_HIGH * ADC_MULTIPLIER)
    VOLTS_WARNING_LOW_ADC = Int(VOLTS_WARNING_LOW * ADC_MULTIPLIER)
    MAX_VOLTS_ADC = Int(MAX_VOLTS * ADC_MULTIPLIER)
    MIN_VOLTS_ADC = Int(MIN_VOLTS * ADC_MULTIPLIER)
    MIN_DISCHARGE_THRESHOLD_VOLTS_ADC = Int(MIN_DISCHARGE_THRESHOLD_VOLTS * ADC_MULTIPLIER)
    SHUNT_MULTIPLIER = R_SENSE / ADC_VREF * ADC_RESOLUTION
    CHARGING_THRESHOLD = INT(CHARGING_THRESHOLD_AMPS * SHUNT_MULTIPLIER)
    MAX_AMPS_ADC = INT(MAX_AMPS * SHUNT_MULTIPLIER)
    MAX_FLOAT_AMPS_ADC = INT(MAX_FLOAT_AMPS * SHUNT_MULTIPLIER)
    MIN_FLOAT_VOLTS_ADC = INT(MIN_FLOAT_VOLTS * ADC_MULTIPLIER)
    TIMER1_PERIOD = INT(31000/SAMPLE_SIZE)
    TIMER1_COUNTER_START = INT(65536 - TIMER1_PERIOD)   '16-bit counter
'    'Timer 2 is an 8-bit counter, so TIMER2_PERIOD *must* be <= 255
'    'Also, Timer 2 interrupts on match, not overflow, so counts up from 0 to
'    ' match value (= TIMER2_PERIOD)
'    TIMER2_PERIOD = INT(31000/(MAX_DUTY_CYCLE * PWM_FREQ))
    'battery level is in ADCbits-seconds
    ' 1Ah * ADCbits/amp * 3600 sec/ hr
    MAX_BATTERY_LEVEL = INT(BATTERY_AH * R_SENSE / ADC_VREF * ADC_RESOLUTION *  3600)
#endscript

#define battery_amps_adc    adc_averages(BATTERY)
'Setup Serial port
'#define USART_BAUD_RATE 19200 
'define USART_BAUD_RATE 115200
#define USART_BAUD_RATE 9600
' Note: USART_TX_BLOCKING will not read input properly!
'#define USART_TX_BLOCKING
#define USART_BLOCKING
#define USART_DELAY OFF
#define SerInPort portb.7       'USART Rx pin
#define SerOutPort portb.6      'USART Tx pin
' PWM channels controllng LED color
#define GREEN_PWM 6
#define RED_PWM 5
'device ON/OFF ports
#define GENERATOR_OFF   portA.6 
#define SOCKET1_ON      portA.3 
#define SOCKET2_ON      portD.2
#define SOCKET3_ON      portA.4
#define SOCKET4_ON      portC.5
#define SOCKET5_ON      portA.5
#define SOCKET6_ON      portD.4
#define SOCKET7_ON      portE.0
#define SOCKET8_ON      portC.3
#define FAN_ON          portC.0
' LED control ports
#define GREEN_LED       portD.7
#define RED_LED         portA.7
'device ADC ports
#define SOCKET1_A1      portB.5
#define SOCKET1_A2      portB.4
#define SOCKET2_A1      portD.3
#define SOCKET2_A2      portC.4
#define SOCKET3_A1      portB.3
#define SOCKET3_A2      portB.2
#define SOCKET4_A1      portC.6
#define SOCKET4_A2      portC.7
#define SOCKET5_A1      portB.1
#define SOCKET5_A2      portB.0
#define SOCKET6_A1      portD.5
#define SOCKET6_A2      portD.6
#define SOCKET7_A1      portA.0
#define SOCKET7_A2      portA.1
#define SOCKET8_A1      portD.1
#define SOCKET8_A2      portD.0
#define FAN_A1          portC.2
#define FAN_A2          portC.1
#define BATTERY_A1      portE.2
#define BATTERY_A2      portE.1
                        
'analog pins
#define SOCKET1_A1_ADC  ANB5
#define SOCKET1_A2_ADC  ANB4
#define SOCKET2_A1_ADC  AND3
#define SOCKET2_A2_ADC  ANC4
#define SOCKET3_A1_ADC  ANB3
#define SOCKET3_A2_ADC  ANB2
#define SOCKET4_A1_ADC  ANC6
#define SOCKET4_A2_ADC  ANC7
#define SOCKET5_A1_ADC  ANB1
#define SOCKET5_A2_ADC  ANB0
#define SOCKET6_A1_ADC  AND5
#define SOCKET6_A2_ADC  AND6
#define SOCKET7_A1_ADC  ANA0
#define SOCKET7_A2_ADC  ANA1
#define SOCKET8_A1_ADC  AND1
#define SOCKET8_A2_ADC  AND0
#define FAN_A1_ADC      ANC2
#define FAN_A2_ADC      ANC1
#define BATTERY_A1_ADC  ANE2
#define BATTERY_A2_ADC  ANE1

' Used to set LED PWM
#define GREEN_PWM_ID    2
#define RED_PWM_ID      1

' ----- Hardware Settings
'set fixed voltage regulator to 2.048 V
' (ADC resolution = 4096 / 2048 mV = 2 bits/mV)
FVRInitialize( FVR_2X )     
wait while FVRIsOutputReady = false
' FVR & Vss are set as default voltage references, included them here just
' to be sure
'ADREF.0 = 1     'ADC +REF = 11 (FVR)
'ADREF.1 = 1
'ADREF.4 = 0     'ADC -REF = 0 (Vss)
ADREF.4 = 1     'ADC NREF = 1 (Vref- pin)

' set I/O pin directions
Dir GREEN_LED out
Dir RED_LED out
Dir SOCKET1_ON out
Dir SOCKET2_ON out
Dir SOCKET3_ON out
Dir SOCKET4_ON out
Dir SOCKET5_ON out
Dir SOCKET6_ON out
Dir SOCKET7_ON out
Dir SOCKET8_ON out
Dir FAN_ON out
Dir GENERATOR_OFF out
Dir SerInPort in        
Dir SerOutPort out     
' analog ports (for current sense resistors)
Dir SOCKET1_A1 in
Dir SOCKET2_A1 in
Dir SOCKET3_A1 in
Dir SOCKET4_A1 in
Dir SOCKET5_A1 in
Dir SOCKET6_A1 in
Dir SOCKET7_A1 in
Dir SOCKET8_A1 in
Dir FAN_A1 in
Dir BATTERY_A1 in
Dir SOCKET1_A2 in
Dir SOCKET2_A2 in
Dir SOCKET3_A2 in
Dir SOCKET4_A2 in
Dir SOCKET5_A2 in
Dir SOCKET6_A2 in
Dir SOCKET7_A2 in
Dir SOCKET8_A2 in
Dir FAN_A2 in
Dir BATTERY_A2 in

' ----- Variables
Dim i, j, k, m, temp_byte, debug_byte As Byte
'debug_byte = 0
Dim blink_counter, sample_counter, pause_timer As Byte
Dim overvolt_counter, duty_cycle_counter As Byte
Dim is_ready_to_receive As Byte
Dim input_string As String
Dim response_string As String
' array storing the current setting of the device (ON, OFF, AUTO_OFF, or PWM_MODE)
' GENERATOR_RELAY is the last element in the power_setting array
Dim power_settings(GENERATOR_RELAY) As Byte
' array storing the current state of the device (ON or OFF)
' normally all ON except when timeouts are exceeded
DIM device_status(DEVICE_COUNT) As Byte
' PWM is available on sockets 3-6 and the fan
' these values are constant during execution
DIM is_pwm_device(DEVICE_COUNT) As Byte
is_pwm_device = 0,0,1,1,1,1,0,0,1   
' stores the current duty cycle setting of the device (0 - MAX_DUTY_CYCLE)
Dim pwm_mode_duty_cycle(DEVICE_COUNT) As Word
' arrays used in ADC calculations
' BATTERY is the last element in the adc array
' low_side = ground side of current sense resistor
' high_side = load or battery side of current sense resistor
Dim low_side_adcs(BATTERY) 
Dim high_side_adcs(BATTERY)
Dim sum_low_side_adcs(BATTERY), sum_high_side_adcs(BATTERY) As Word
Dim debug_sum_low_side_adcs(BATTERY), debug_sum_high_side_adcs(BATTERY) As Word
Dim adc_averages(BATTERY) As Integer
Dim adc_offsets(BATTERY) As Integer
Dim average_volts, shutdown_volts As Word
Dim shutdown_battery_amps_adc As Integer
Dim battery_level, temp_long As Long
Dim temp_word, drain_timer, max_battery_drain_time, soc_recalibration_timer As Word
Dim timer1_start_value, timer1_end_value, sample_time As Word
Dim seconds As Long

#define VOLTS_ADC_CHANNEL low_side_adcs(BATTERY)
#ifdef TEST_MODE 0
    ' analog pins are stored in arrays for reading
    low_side_adcs = SOCKET1_A1_ADC, SOCKET2_A1_ADC, SOCKET3_A1_ADC, SOCKET4_A1_ADC, SOCKET5_A1_ADC, SOCKET6_A1_ADC, SOCKET7_A1_ADC, SOCKET8_A1_ADC, FAN_A1_ADC, BATTERY_A1_ADC 
    high_side_adcs = SOCKET1_A2_ADC, SOCKET2_A2_ADC, SOCKET3_A2_ADC, SOCKET4_A2_ADC, SOCKET5_A2_ADC, SOCKET6_A2_ADC, SOCKET7_A2_ADC, SOCKET8_A2_ADC, FAN_A2_ADC, BATTERY_A2_ADC
#endif
#ifdef TEST_MODE 1
    ' fake ADC values are stored here for testing purposes
    Dim test_low_side_adc_values(BATTERY), test_high_side_adc_values(BATTERY) As Word
    For i = 1 to BATTERY
        test_low_side_adc_values(i) = 0
        Select Case i
            Case BATTERY
                test_low_side_adc_values(i) = 780
                test_high_side_adc_values(i) = 780 
            Case Else
                test_high_side_adc_values(i) = 0
        End Select
    Next
#endif

max_battery_drain_time = 300        'default shut off is 5 minutes

'Initialize variables using subroutine so they can be reset after sleep
InitializeVariables()

'Setup PWM
InitializePWM()

'Setup Timer
InitializeInterruptLoopTimer()

#ifdef TEST_MODE 1
    ' simulate battery charging @ beginning
    test_high_side_adc_values(BATTERY) = 860 'high_side > low_side --> battery charging
   ' test_high_side_adc_values(BATTERY) = 760 'high_side < low_side --> battery discharging
    ' simulate load on another device
    test_high_side_adc_values(1) = 10 
#endif

'*************************************************************************
'Main program commences here.. everything before this is board setup

Do
    clrwdt          'assembly code must be lower case!
    HSerGetString input_string 
    ' the command is the first character of the input string
    ' response_string is appended to the next outgoing data stream
    Select Case input_string(1)
        Case "!"
            ' shutdown now, e.g. to reboot when programming
            ShutdownDevices()
        Case "A"
            ' set device to "AUTO_OFF", using second character as index
            response_string = ChangePowerSetting(input_string(2), AUTO_OFF)
        Case "C"
            ' clear reset status bits
            PCON0 = 0b00111111
            response_string = "OK"
        Case "D"
            ' turn device off ("down"), using second character as index
            response_string = ChangePowerSetting(input_string(2), OFF)
        Case "G"
            ' wait until computer is up and ready to receive data
            is_ready_to_receive = 1
            HSerPrint "ACK"
            HSerPrintCRLF
        Case "P"
            ' set device to PWM mode
            response_string = ChangePowerSetting(input_string(2), PWM_MODE)
        Case "R"
            ' re-set the current number of amp-hours in the battery
            ' 
            temp_long = StrToLong(Mid(input_string, 2))
            If temp_long = 0 Then
                battery_level = MAX_BATTERY_LEVEL
                response_string = "Too large, set to max level"
            Else
                battery_level = temp_long
                response_string = "OK"
            End If
        Case "T"
            ' change the max. allowed shutdown time
            temp_word = StrToWord(Mid(input_string, 2))
            If temp_word > MIN_DRAIN_TIME Then
                max_battery_drain_time = temp_word
                response_string = "OK"
            Else
                response_string = "ERROR"
            End If
        Case "U"
            ' turn device on ("up"), using second character as index
            response_string = ChangePowerSetting(input_string(2), ON)
#ifdef TEST_MODE 1
        Case "V"
            ' set ADC value when in test mode
            ' first digit determines battery, socket, or fan
            ' second character deterimines whether high (H) or low (L) (ground) side value 
            ' remainder determines value to set
            ' e.g. "1H200" sets power socket 1 high side ADC value to 200 &
            ' "0L800" sets battery low side ADC value to 800
            temp_word = Val(Mid(input_string,4))
            temp_byte = Val(Mid(input_string,2,1))
            If temp_word > 4095 Then
                response_string = "Value > 4095"
            Else If temp_byte > DEVICE_COUNT Then
                response_string = Str(temp_byte) + " is an invalid device #"
            Else
                Select Case input_string(3)
                    Case "L"
                        If temp_byte = 0 Then
                            test_low_side_adc_values(BATTERY) = temp_word
                        Else
                            test_low_side_adc_values(temp_byte) = temp_word
                        End If
                        response_string =  "OK"
                    Case "H"
                        If temp_byte = 0 Then
                            test_high_side_adc_values(BATTERY) = temp_word
                        Else
                            test_high_side_adc_values(temp_byte) = temp_word
                        End If
                        response_string = "OK"
                    Case Else
                        response_string = "not H or L"
                End Select
            End If
#endif
        Case "%"
            ' set PWM_MODE duty cycle for a device
            ' first digit determines which socket or fan
            ' remainder determines value to set
            ' max value is MAX_DUTY_CYCLE (256)
            ' e.g. "%3128" sets power socket 3 to 50% (128 is 50% of 256)
            j = input_string(2) - 48 'convert character to numeric value
            temp_word = Val(Mid(input_string,3))
            If is_pwm_device(j) = 0 Then
                response_string = "INVALID DEVICE"
            Else If temp_word > MAX_DUTY_CYCLE Then
                response_string = "TOO LARGE"
            Else
                SetDutyCycle(j, temp_word)
                pwm_mode_duty_cycle(j) = temp_word
                response_string = "OK"
            End If

        Case Else
            response_string = "Invalid - '" + CHR(input_string(1)) + "'"
    End Select
Loop

Sub InitPPS
    ' Configure serial port pins
    'input peripherals map to pin registers,
    'output pins map to peripheral registers
    'Module: UART1
    U1RXPPS = 0x000F               'RB7 > Rx
    RB6PPS = 0x0013                'RB6 > Tx

    'set socket and fan pins to PWM (see table 17-2, p 283 in datasheet)
    RA4PPS = 0x10                  'SOCKET3_ON: RA4 > PWM8
    RC5PPS = 0x0A                  'SOCKET4_ON: RC5 > CCP2
    RA5PPS = 0x0F                  'SOCKET5_ON: RA5 > PWM7
    RD4PPS = 0x0B                  'SOCKET6_ON: RD4 > CCP3
    RC0PPS = 0x09                  'FAN_ON: RC0 > CCP1
End Sub

Sub PWMPPS
    ' sets LED pins to PWM for strobing
    RD7PPS = 0x0E                  'GREEN_LED: RD7 > PWM6
    RA7PPS = 0x0D                  'RED_LED: RA7 > PWM5
End Sub

Sub GPIOPPS
    'sets LED pins to digital I/O for blinking
    RD7PPS = 0x00                  'GREEN_LED: RD7 > GPIO
    RA7PPS = 0x00                  'RED_LED: RA7 > GPIO
End Sub

Sub InitializeVariables
    ' zeroes flags, sums, and counters, and start with everything OFF
    is_ready_to_receive = 0     'wait until computer is up before sending data,
                                'otherwise will block and hang
    ' wait a few seconds with the LED lit to indicate it's restarting
    RED_LED = ON
    For i = 1 to 5:
        Wait 2 sec
        clrwdt
    Next
    RED_LED = OFF

    For i = 1 to BATTERY
        adc_averages(i) = 0
        sum_low_side_adcs(i) = 0
        sum_high_side_adcs(i) = 0
        'set state at startup
        TurnOnOff(i, OFF)   'everything is OFF
        'set state when user begins pedaling
        '(Note: these are applied when battery starts charging, when user
        'begins pedaling.  This happens in Case statement in InterruptLoop)
        If i = GENERATOR_RELAY Then
            power_settings(GENERATOR_RELAY) = OFF 'relay is OFF --> generator is ON
        Else If i = FAN Then   
            power_settings(FAN) = AUTO_OFF    'no need to keep fan always on
        Else
            power_settings(i) = ON      'all other devices
        End If
        If i <> BATTERY Then
            ' start with 0% duty cycle on all PWM-capable devices
            ' (non-PWM devices will always be 0%) 
            pwm_mode_duty_cycle(i) = 0
            'device_status(i) = OFF
        End If
    Next

    ' set drain_timer to non-zero value so strobing begins at startup.
    ' Strobing will cease once max_battery_drain_time is reached.
    drain_timer = LED_START_STROBE      'tracks how long battery has drained
    shutdown_volts = 0          'volts @ shutdown
    shutdown_battery_amps_adc = 0   'battery_amps_adc @ shutdown
    blink_counter = 0           'counts blinks for blinking pattern
    soc_recalibration_timer = 0       'determines when to recalibrate state of charge
    pause_timer = 0             'pause period while overvoltage

    ' calculate offsets
    'if system resets while someone is pedalling, the battery offset will be
    ' wrong.  So we'll check to see if the battery is charging first
    For j = 1 to SAMPLE_SIZE    
        clrwdt
        ReadAndSum(BATTERY)
        wait 10 ms
    Next
    'if the battery is charging, then the relay needs to disconnect the
    ' generator to stop it from happening. 
    ' The relay will draw a small amount (~33 ma) of battery current, so the 
    ' offset won't be perfect but better than otherwise! 
    If CalcAvg(sum_low_side_adcs(BATTERY), sum_high_side_adcs(BATTERY)) > CHARGING_THRESHOLD Then
        ' energizing the generator relay disconnects the generator
        TurnOnOff(GENERATOR_RELAY, ON)    
    End If
    sum_low_side_adcs(BATTERY) = 0
    sum_high_side_adcs(BATTERY) = 0

    GPIOPPS()
    ' blink three times as an indicator, and to allow capacitors to reach full
    ' charge
    For i = 1 to 3:
        GREEN_LED = ON
        Wait 1 sec
        GREEN_LED = OFF
        Wait 1 sec
        clrwdt
    Next
    ' allow more time for voltage to stabilize after blinking
    Wait 3 sec  
    For j = 1 to SAMPLE_SIZE
        clrwdt
        For i = 1 to BATTERY
            ReadAndSum(i)
            wait 10 ms
        Next
    Next
    For i = 1 to BATTERY
        clrwdt
        If i = BATTERY Then
            average_volts = FnLSR(sum_low_side_adcs(i), RSHIFT) 
        End If
        adc_offsets(i) = CalcAvg(sum_low_side_adcs(i), sum_high_side_adcs(i))
        ' Offset value should be close to zero, except when
        ' server, computer, modem, and/or monitor may be running
        ' after reflashing the firmware.
        ' This handles that circumstance.
        If Abs(adc_offsets(i)) > 3 Then
            adc_offsets(i) = 0
        End If
        sum_low_side_adcs(i) = 0
        sum_high_side_adcs(i) = 0
    Next

    ' estimate battery charge level using voltage level prior to start
    If average_volts < MIN_VOLTS_ADC Then   
        'must have lookup value >= 0 in all cases!
        average_volts = MIN_VOLTS_ADC - 1   
    End If
    ReadTable StateOfCharge, (average_volts - MIN_VOLTS_ADC + 1), battery_level
    average_volts = 0
    TurnOnOff(GENERATOR_RELAY, OFF)   'make sure relay is off to engage generator
End Sub

Sub InitializePWM
    'Frequencies below 500 Hz with maximum duty cycle
    'values evenly divisible by 64:
    '                        TMR2
    '  PWM freq      T2PR  prescale
    '---------------------------------------------------------------------
    '    122 Hz       255       128
    '    130 Hz       239       128
    '    140 Hz       223       128
    '    150 Hz       207       128
    '    163 Hz       191       128
    '    178 Hz       175       128
    '    195 Hz       159       128
    '    217 Hz       143       128
    '    244 Hz       127       128
    '    260 Hz       239        64
    '    279 Hz       111       128
    '    300 Hz       207        64
    '    326 Hz        95       128
    '    355 Hz       175        64
    '    391 Hz        79       128
    '    434 Hz       143        64
    '    488 Hz        63       128

    ' Use Timer 2 on PWM 8 & 7 (sockets 3 & 5) and PWM 6 & 5 (green and red LEDs)
    CCPTMRS1 = 0b01010101
    ' Set CCPxPWM on, use left-aligned format, and select PWM mode
    CCP1CON = 0b10011100
    CCP2CON = 0b10011100
    CCP3CON = 0b10011100
    ' Load T2PR & T4PR with PWM period value
    T2PR = 127 
    ' preload duty_cycle_H and duty_cycle_L registers 
    For i = 3 to DEVICE_COUNT
        SetDutyCycle(i, 0)
    Next
    ' Clear Timer 2 interrupt flag bit
    PIR4.TMR2IF = 0
    ' Set Timer 2 clock source to FOSC/4
    T2CLKCON = 1 
    ' enable Timer 2, set pre-scaler to 1:128, post-scaler to 1:1 to get 244 Hz
    T2CON = 0b11110000
    ' enable PWMs 5-8 (CCP PWMs were enabled above in CCPxCON registers)
    PWM5CON.7 = ON
    PWM6CON.7 = ON
    PWM7CON.7 = ON
    PWM8CON.7 = ON
End Sub

Sub InitializeInterruptLoopTimer
    On Interrupt Timer1Overflow Call InterruptLoop
    InitTimer1(LFINTOSC, PS1_1)
    SetTimer(1, TIMER1_COUNTER_START)
    StartTimer(1)
End Sub

Sub InterruptLoop
    ' overflow flag is auto cleared
    TMR1IF = 0
    SetTimer(1, TIMER1_COUNTER_START)
    ' better to clear the WDT here than in the blocking main loop
    clrwdt
    ' read data and add to subtotals
    For i = 1 To BATTERY
        ReadAndSum(i)
    Next
    sample_counter++ 
    ' Do the following once per second
    If sample_counter >= SAMPLE_SIZE Then

        CalculateADCAverages()

        soc_recalibration_timer = UpdateBatterySOC(average_volts, battery_amps_adc, soc_recalibration_timer)

        pause_timer = HandleOvercharging(average_volts, battery_amps_adc, soc_recalibration_timer, pause_timer)

        drain_timer = HandleUndercharging(average_volts, battery_amps_adc, drain_timer)

        seconds++
        sample_counter = 0
        sample_time = 0
    End If

    SendSerialData(sample_counter)

    ' test to see how long it takes to sample data
    If (Timer1 - timer1_start_value) > sample_time Then
        sample_time = Timer1 - timer1_start_value
    End If

    BlinkOrStrobeLED(average_volts, battery_amps_adc, drain_timer, sample_counter, pause_timer)

End Sub

Sub CalculateADCAverages()
    'calculate average voltage and device values over last period
    For i = 1 To BATTERY
        If i = BATTERY Then
            'use battery's current sense resistor input value to
            'measure battery voltage, since it's at the same potential as the battery
            average_volts = FnLSR(sum_low_side_adcs(i), RSHIFT)
        End If
        ' calculate value for device as difference between it's ADCs and
        ' subtracting off it's zero offset
        'adc_averages(i) = CalcAvg(sum_low_side_adcs(i), sum_high_side_adcs(i)) - adc_offsets(i)
        adc_averages(i) = CalcAvg(sum_low_side_adcs(i), sum_high_side_adcs(i))
        debug_sum_low_side_adcs(i) = sum_low_side_adcs(i)
        debug_sum_high_side_adcs(i) = sum_high_side_adcs(i)
        sum_high_side_adcs(i) = 0
        sum_low_side_adcs(i) = 0
    Next
End Sub

Function UpdateBatterySOC(average_volts As Word, battery_amps_adc As Integer, c_timer As Word) As Word
    ' Update the battery state-of-charge using coulomb counting, and increments
    ' the state-of-charge calibration timer if the battery is nearing full charge
    If battery_amps_adc < 0 Then
        ' Battery discharging.
        ' Convert battery_amps_adc to unsigned long to match battery_level's type.
        temp_long = 0 + [long](-1 * battery_amps_adc)
        ' The battery_level should never fall this low, but just in case it
        ' somehow does this keeps it from overflowing.
        If temp_long > battery_level Then
            battery_level = 0
        Else
            battery_level = battery_level - temp_long
        End If
        ' Reset the battery state-of-charge (SOC) calibration timer.
        c_timer = 0
    Else
        ' Battery charging.
        battery_level = battery_level + [long]battery_amps_adc
        If battery_level > MAX_BATTERY_LEVEL Then
            battery_level = MAX_BATTERY_LEVEL
        End If
        If average_volts >= MIN_FLOAT_VOLTS_ADC and battery_amps_adc <= MAX_FLOAT_AMPS_ADC Then
            ' Increment the SOC calibration timer since the battery voltage
            ' is nearing it's limit.
            c_timer++
            If soc_recalibration_timer > SOC_RESET_TIME_PERIOD Then
                ' recalibrate the battery level to full
                battery_level = MAX_BATTERY_LEVEL
                c_timer = 0
            End If
        End If
    End If
    UpdateBatterySOC = c_timer
End Function

Function HandleOvercharging(average_volts As Word, battery_amps_adc As Integer, soc_recalibration_timer As Word, p_timer As Byte) As Byte
    ' If overvoltage or battery is overcharging, open relay for a few seconds.
    ' Doing this as a function rather than a subroutine to make it explicit 
    ' in the interrupt routine where pause_timer is set.
    If average_volts > MAX_VOLTS_ADC or battery_amps_adc > MAX_AMPS_ADC Then
        ' could be a transient at startup, so don't open relay unless it
        ' continues
        overvolt_counter++
        ' disconnect generator to prevent battery damage and 
        ' inverter shutoff due to overvoltage
        If overvolt_counter >= MAX_OVERVOLT_PERIOD Then
            TurnOnOff(GENERATOR_RELAY, ON)    'relay trips off generator
            power_settings(GENERATOR_RELAY) = ON
            p_timer = 1
            shutdown_volts = average_volts
            shutdown_battery_amps_adc = battery_amps_adc
        End If
    Else 
        overvolt_counter = 0
        If p_timer > 0 Then
            'still in waiting period before restart
            p_timer++
            If p_timer >= RESTART_WAIT_PERIOD Then
                'reconnect generator by turning relay off
                TurnOnOff(GENERATOR_RELAY, OFF)
                power_settings(GENERATOR_RELAY) = OFF
                p_timer = 0
                shutdown_volts = 0
                shutdown_battery_amps_adc = 0
            End If
        End If
    End If
    HandleOvercharging = p_timer
End Function

Function HandleUndercharging(average_volts As Word, battery_amps_adc As Integer, d_timer As Word) As Word
    ' Turn devices off if battery has been draining too long
    ' Doing this as a function rather than a subroutine to make it explicit 
    ' in the interrupt routine where drain_timer is set
    If battery_amps_adc > CHARGING_THRESHOLD or average_volts > MIN_DISCHARGE_THRESHOLD_VOLTS_ADC Then
        If d_timer > 0 Then
            If d_timer >= LED_START_STROBE Then
                'user is beginning to pedal while machine is paused,
                'so need to reset LED pins to digital I/O to blink the LED
                GPIOPPS()
            End If
            For i = 1 to FAN    'exclude generator relay
                'wake up each device that is not set to OFF
                Select Case power_settings(i)
                    Case ON to AUTO_OFF
                        TurnOnOff(i, ON)
                    Case PWM_MODE
                        SetDutyCycle(i, pwm_mode_duty_cycle(i))
                End Select
            Next
            d_timer = 0
        End If
    Else
        ' prevent overflowing counter if draining for long period
        ' which would turn it back on!
        If d_timer < max_battery_drain_time Then
            d_timer++
        End If
        For i = 1 to FAN    'exclude generator relay
            If d_timer >= AUTO_OFF_PERIOD Then
                If power_settings(i) <> ON Then
                    'turn off AUTO OFF  & PWM_MODE devices, leave off OFF devices
                    TurnOnOff(i, OFF)
                Else If d_timer >= max_battery_drain_time Then
                    'turn off ALL devices, including those set to ON 
                    TurnOnOff(i, OFF)
                End If
            End If
        Next
    End If
    HandleUndercharging = d_timer
End Function

Sub SendSerialData(In sample_counter As Byte)
    ' printing is done over several loops to prevent collision with interrupt
    ' and to prevent all devices from coming on at once (which could cause
    ' a reset)
    If is_ready_to_receive > 0 Then
        Select Case sample_counter
            Case 0: SendVolts
            ' Host computer uses 0-based arrays, while GCBASIC are 1-based.
            ' Host stores battery amps as first element, so keys of other elements
            ' match their name (amps[1]=socket #1 amps, etc).  
            ' Therefore send battery amps first to make API more intuitive to host.
            Case 1: SendAmps(BATTERY)   'send battery amps first
            Case 2 To DEVICE_COUNT+1: 
                SendAmps(sample_counter-1)  'send amps of all other devices
            Case DEVICE_COUNT+2: SendTimerStatus
            Case DEVICE_COUNT+3: SendDeviceStatus
            Case DEVICE_COUNT+4: SendPWMDutyCycle
            Case DEVICE_COUNT+5: SendBatteryLevel
            Case DEVICE_COUNT+6: SendResponse
            Case DEVICE_COUNT+7: SendDebuggingData
        End Select
    End If
End Sub

Sub BlinkOrStrobeLED(average_volts As Word, battery_amps_adc As Integer, drain_timer As Word, sample_counter As Byte, pause_timer As Byte)
    ' strobe or blink LED based on amps, volts, and length of time
    ' battery may be discharging
    If drain_timer >= LED_START_STROBE Then
        'put LED pins in PWM mode to get strobe effect
        PWMPPS()
        StrobeLED(sample_counter, drain_timer, average_volts)
    Else If sample_counter % ( SAMPLE_SIZE / BLINK_RATE ) = 0 Then
        ' LED may blink as often as 1/4 sec
        If pause_timer > 0 Then
            ' in shutdown mode here -> use shutdown variables
            BlinkLED(shutdown_volts, shutdown_battery_amps_adc, drain_timer) 
        else
            BlinkLED(average_volts, battery_amps_adc, drain_timer) 
        End If
    End If
End Sub

Function CalcAvg(val1 as Word, val2 as Word) as Integer
    ' Calculates net average difference between ADC values of a current sense 
    ' resistor's input and output values, using the sums of multiple
    ' input and output values
    Dim temp1, temp2 as Word
    ' perform a right shift to take averages
    temp1 = FnLSR(val1, RSHIFT)
    temp2 = FnLSR(val2, RSHIFT)
    ' now calculate the difference between the averages
    CalcAvg = [integer]temp2 - temp1
End Function

Function StrToWord(number_string as String) as Word
    ' convert a number input as string to a word
    StrToWord = 0
    For i = 1 To number_string(0)
        StrToWord = 10 * StrToWord + (number_string(i) - 48)
    Next
End Function

Function StrToLong(number_string as String) as Long
    ' convert a number input as string to a long
    StrToLong = 0
    For i = 1 To number_string(0)
        StrToLong = 10 * StrToLong + (number_string(i) - 48)
    Next
End Function

' data output is broken over multiple subroutines to allow more samples to be
' taken without causing overlaps in the ISR

Sub SendVolts
    ' send the ADC reading for average volts
    HSerPrint average_volts
    HserPrint ","
End Sub

Sub SendAmps(m)
    ' send the ADC reading for the voltage drop across each current 
    ' sense resistor
    HSerPrint adc_averages(m)
    If m = DEVICE_COUNT Then
        HSerPrint ";"
    Else
        HSerPrint ","
    End If
End Sub

Sub SendTimerStatus
    ' send # of seconds battery has been draining and the current maximum
    ' allowed battery drain time setting
    HSerPrint drain_timer
    HSerPrint ","
    HSerPrint max_battery_drain_time
    HSerPrint ";"
End Sub

Sub SendDeviceStatus
    ' send the power setting for each device (off=0, on=1, auto=2)
    HSerPrint power_settings(GENERATOR_RELAY)
    HSerPrint ","
    For m = 1 To FAN
        HSerPrint power_settings(m)
        If m = FAN Then
            HSerPrint ";"
        Else
            HserPrint ","
        End If
    Next
End Sub

Sub SendPWMDutyCycle
    For m = 1 to DEVICE_COUNT
        If is_pwm_device(m) = 1 then
            HSerPrint pwm_mode_duty_cycle(m)
            If m = DEVICE_COUNT Then
                HSerPrint ";"
            Else
                HserPrint ","
            End If
        End If
    Next
End Sub

Sub SendBatteryLevel
    ' send the approximate number of "ADCbit-seconds" in the battery
    HSerPrint battery_level
    HserPrint ","
    HSerPrint MAX_BATTERY_LEVEL
    HserPrint "|"
End Sub

Sub SendResponse
    ' send response from the latest command (if any)
    HSerPrint response_string
    response_string = ""
    HserPrint "|"
End Sub

Sub SendDebuggingData
    ' send any desired debugging data (calibration constants, etc.)
    ' along with the CRLF for all data
    ' NOTE: Beware of sending too much data here--receive commands become buggy!
    HserPrint debug_sum_low_side_adcs(BATTERY)
    HserPrint ","
    HserPrint debug_sum_high_side_adcs(BATTERY)
    HSerPrintCRLF
End Sub

Function ChangePowerSetting(In device_number_char As Byte, In mode As Byte) as String
    ' Change power_setting of device and turn it on/off
    ' 1 is socket #1, 2 is socket #2, etc.
    i = device_number_char - 48  ' convert character to digit
    If (i > 0) and (i <= DEVICE_COUNT) Then
        If mode = PWM_MODE Then
            ' only allow PWM on PWM-capable devices
            If is_pwm_device(i) = 1 Then
                SetDutyCycle(i, pwm_mode_duty_cycle(i))
                power_settings(i) = mode
                ChangePowerSetting = "OK"
            Else
                ChangePowerSetting = "not a PWM device"
            End If
        Else
            power_settings(i) = mode
            If mode = AUTO_OFF Then
                mode = ON
            End If
            TurnOnOff(i, mode)
            ChangePowerSetting = "OK"
        End If
    Else
        ChangePowerSetting = "invalid device #"
    End If
End Function

Sub TurnOnOff(In device_number As Byte, In level As Byte) 
    ' Turns device on or off
    ' SetWith sets the bit as long as the second parameter is non-zero,
    ' so "AUTO_OFF" devices will turn on when it's passed as a parameter
    Select Case device_number
        Case 1
            SetWith(SOCKET1_ON, level)
        Case 2
            SetWith(SOCKET2_ON, level)
        Case 3 to 6
            If level = ON Then
                SetDutyCycle(i, MAX_DUTY_CYCLE)
            Else
                SetDutyCycle(i, 0)
            End If
        Case 7
            SetWith(SOCKET7_ON, level)
        Case 8
            SetWith(SOCKET8_ON, level)
        Case FAN
            If level = ON Then
                SetDutyCycle(i, MAX_DUTY_CYCLE)
            Else
                SetDutyCycle(i, 0)
            End If
        Case GENERATOR_RELAY
            SetWith(GENERATOR_OFF, level)
    End Select
End Sub

Sub SetDutyCycle(In device As Byte, In duty_cycle As Word)
    Dim temp_duty_cycle As Word
    ' duty_cycle ranges from 0-256, so scale by (T2PR + 1) * 4/64 = 2, then left-shift by 6
    ' (2 ^ 6 = 64) to left-align format
    ' NOTE: additional adjustment is required if scale factor = 16, since the 10-bit
    '   duty cycle will overflow when duty_cycle = 64.
    temp_duty_cycle = duty_cycle * 64 * 2
    Select Case device
        Case RED_PWM_ID
            'PWM 5
            PWM5DCH = temp_duty_cycle_H
            PWM5DCL = temp_duty_cycle
        Case GREEN_PWM_ID 
            'PWM 6
            PWM6DCH = temp_duty_cycle_H
            PWM6DCL = temp_duty_cycle
        Case 3
            'PWM 8
            PWM8DCH = temp_duty_cycle_H
            PWM8DCL = temp_duty_cycle
        Case 4
            'CCPPWM 2
            CCPR2H = temp_duty_cycle_H
            CCPR2L = temp_duty_cycle
        Case 5
            'PWM 7
            PWM7DCH = temp_duty_cycle_H
            PWM7DCL = temp_duty_cycle
        Case 6
            'CCPPWM 3
            CCPR3H = temp_duty_cycle_H
            CCPR3L = temp_duty_cycle
        Case DEVICE_COUNT
            'CCPPWM 1
            CCPR1H = temp_duty_cycle_H
            CCPR1L = temp_duty_cycle
    End Select
End Sub

Sub BlinkLED(In volts As Integer, In battery_amps_adc As Integer, In drain_timer As Word) 
    Dim battery_status as Byte
    ' bit patterns below define LED state 
    ' each one byte = 1 sec
    ' each bit defines whether green and/or red element is on or off
    ' pattern is GRGRGRGR where G = green led, R = red led
    #define BATTERY_CHARGING        0b10101010  'solid green
    #define TOO_FAST                0b10011001  'blink fast green/red
    #define BATTERY_DISCHARGING     0b11111111  'solid yellow
    #define BATTERY_WARNING         0b01010000  'blink red
    #define BATTERY_VOLTAGE_LOW     0b01000000  'brief red blink
    #define ASLEEP                  0           'no blinking when asleep
    If volts > VOLTS_WARNING_HIGH_ADC or battery_amps_adc > MAX_AMPS_ADC Then
        battery_status = TOO_FAST
    Else If battery_amps_adc < CHARGING_THRESHOLD Then
        If volts < MIN_VOLTS_ADC Then
            battery_status = BATTERY_VOLTAGE_LOW
        Else If drain_timer > AUTO_OFF_PERIOD Then
            battery_status = ASLEEP 
        Else If drain_timer > LED_DRAIN_PERIOD Then
            battery_status = BATTERY_WARNING
        Else
            battery_status = BATTERY_DISCHARGING
        End If
    Else
        battery_status = BATTERY_CHARGING
    End If
    blink_counter++
    If blink_counter >= BLINK_RATE Then
        blink_counter = 0
    End If
    ' select LED bits by right-shifting battery_status
    ' cannot set using equality due to Great Cow BASIC language -->
    '    must use SetWith instead due to variable parameter
    SetWith(GREEN_LED, FnLSR(battery_status, 2 * blink_counter + 1) & 0x01)
    SetWith(RED_LED, FnLSR(battery_status, 2 * blink_counter) & 0x01)
End Sub

Sub StrobeLED(In sample_counter As Byte, In drain_timer As Word, In adc_volts As Word)
    ' strobe LED based on voltage
    ' LED increases in brightness for 1 sec, decreases in brightness for 1 sec, 
    '   then pauses for remainder of STROBE_PERIOD
    ' color is determined by volts ADC reading:
    #define STROBE_PERIOD 8     'seconds for a complete up-down-off strobe cycle
    Dim red_multiplier, green_multiplier as Word
    Dim red_brightness, green_brightness, strobe_counter, pwm_multiplier as Word
    If drain_timer = max_battery_drain_time Then
        'turn strobe off after all has shutdown
        SetDutyCycle(RED_PWM_ID, 0) 'red
        SetDutyCycle(GREEN_PWM_ID, 0) 'green
        Return
    End If
    ' calculate color at t=1sec based on the current voltage reading
    #script
        'ADC_MULTIPLIER = ~58.82
        GREEN_VOLTS = INT(13.0 * ADC_MULTIPLIER)    '13.0 -> 765
        YELLOW_VOLTS = INT(12.5 * ADC_MULTIPLIER)   '12.5 -> 735
        ORANGE_VOLTS = INT(11.7 * ADC_MULTIPLIER)   '11.7 -> 688
    #endscript
    If adc_volts > GREEN_VOLTS Then
        green_multiplier = 8
        red_multiplier = 0
    Else If adc_volts > YELLOW_VOLTS Then
        green_multiplier = 8
        red_multiplier = 1
    Else If adc_volts > ORANGE_VOLTS Then
        green_multiplier = 2
        red_multiplier = 1
    Else    
        ' full red
        green_multiplier = 0
        red_multiplier = 2
    End If
    ' adjust the brightness based on the current sampling time
    strobe_counter = drain_timer % STROBE_PERIOD
    Select Case strobe_counter
        Case 1: pwm_multiplier = sample_counter                'increasing brightness 
        Case 2: pwm_multiplier = SAMPLE_SIZE-sample_counter    'decreasing brightness 
        Case Else: pwm_multiplier = 0                          'LED off
    End Select
    ' set color based on voltage AND time
    ' assumes 0-256 duty cycle
    green_brightness = green_multiplier * pwm_multiplier
    red_brightness = red_multiplier * pwm_multiplier
    SetDutyCycle(RED_PWM_ID, red_brightness) 
    SetDutyCycle(GREEN_PWM_ID, green_brightness) 
End Sub

Sub ReadAndSum(In i As Byte)
#ifdef TEST_MODE 0
    sum_low_side_adcs(i) += ReadAD12(low_side_adcs(i), True)
    sum_high_side_adcs(i) += ReadAD12(high_side_adcs(i), True)
#endif
#ifdef TEST_MODE 1
    Dim throwaway As Word
    ' read two ADCs to simulate how long it takes
    throwaway = ReadAD12(BATTERY_A1_ADC, True)
    throwaway = ReadAD12(BATTERY_A2_ADC, True)
    ' now use the simulated values instead
    sum_low_side_adcs(i) += test_low_side_adc_values(i)
    sum_high_side_adcs(i) += test_high_side_adc_values(i)
#endif
End Sub

Sub ShutdownDevices
End Sub

' Table to determine battery state of charge based on voltage level
' Returns estimated ADCbit-seconds remaining
' amp-hours * ADCbits/amp * 3600 sec/ hr
' first value (0%): 647 11.000 V 0
' last value (100%): 852 14.500 V 2880000
' total rows:  206
Table StateOfCharge as Long
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    0
    6260
    12521
    18782
    25043
    31304
    37565
    43826
    50086
    56347
    62608
    68869
    75130
    81391
    87652
    93913
    100173
    106434
    112695
    118956
    125217
    131478
    137739
    144000
    150260
    156521
    162782
    169043
    175304
    181565
    187826
    194086
    200347
    206608
    212869
    219130
    225391
    231652
    237913
    244173
    250434
    256695
    262956
    269217
    275478
    281739
    288000
    316800
    345600
    374400
    403200
    432000
    460800
    489600
    518400
    547200
    576000
    624000
    672000
    720000
    768000
    816000
    864000
    912000
    960000
    1008000
    1056000
    1104000
    1152000
    1440000
    1728000
    1824000
    1920000
    2016000
    2073600
    2131200
    2188800
    2246400
    2592000
    2707200
    2822400
    2826514
    2830628
    2834742
    2838857
    2842971
    2847085
    2851200
    2855314
    2859428
    2863542
    2867657
    2871771
    2875885
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
    2880000
End Table
